#!/bin/sh

MOD_LIST="$1"
DEST="$2"

NEW_MODS=0

if [ -z "$MOD_LIST" ] || [ -z "$DEST" ] ; then
	echo "Usage: $0 <mod_list_file> <dest_dir>"
	exit 1
fi

echo "Downloading mods to $DEST...\n"

# Get mods excluding comments in file
MODS="$(cat $MOD_LIST | grep -v -E '^#' )"

for mod in $MODS ; do
	file="$(basename $mod)"
	if ! stat "$DEST/$file" > /dev/null 2>&1 ; then
		echo "Downloading $file..."
		wget "$mod" -O "$DEST/$file" > /dev/null 2>&1
		if [ $? -ne 0 ] ; then
			echo "ERROR downloading $file"
			rm "$DEST/$file" 2> /dev/null
			exit 1
		fi
		NEW_MODS=1
	else
		echo "$file already present. Skipping..."
	fi
done

echo ""

for item in $(ls $DEST) ; do
	if ! echo $MODS | grep $item > /dev/null ; then
		rm $DEST/$item
		echo "Removed $item"
	fi
done

if [ $NEW_MODS -eq 0 ] ; then
	echo "\nNo new mods installed."
else
	echo "\nNew mods have been installed."
fi
