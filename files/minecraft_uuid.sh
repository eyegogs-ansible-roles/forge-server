#!/bin/sh

USERNAME="$1"
if [ -z "$USERNAME" ] ; then
	echo "ERROR: Must provide username - $0 <username>"
	exit 1
fi

ID="$(curl https://api.mojang.com/users/profiles/minecraft/$USERNAME 2> /dev/null | grep 'id' | sed -E 's/.+"(.+)".+/\1/')"

if [ -z "$ID" ] ; then
	echo "ERROR: User '$USERNAME' not found."
	exit 1
else
	echo "$ID" | sed -E 's/(.{8})(.{4})(.{4})(.{4})(.+)/\1-\2-\3-\4-\5/'
fi
