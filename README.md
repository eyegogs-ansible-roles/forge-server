# Ansiblised Minecraft Forge Server

## Intro
An Ansible role to deploy instances of [Eyegog's forge server container](https://gitlab.com/eyegogs-docker-containers/forge-server-container).

There are some things you should know before using this role:
- This repository is maintained on lazy-sys-admin basis. MRs are welcome :)
- This role **assumes docker is installed on the remote**
- The query server and rcon server are **not yet implemented**
- The forge servers will rearrange the order of the `server.properties` file and also add timestamps as comments to the top of the file. Which makes it difficult to template `server.properties` and retain idempotency. So by default, if a `server.properties` file already exists for a given server, it will not be templated. This behaviour can be disabled with `forge_overwrite_server_properties`.
- As config files such as `banned-player.json`, `whitelist.json`, `ops.json` etc... are living, (i.e. They can be changed by the server instance itself using in-game commands such as `/whitelist`) and so they are **not overwritten** if already present to preserved idempotency

## Installation
To install the role via command-line:
```
ansible-galaxy install git+https://gitlab.com/eyegogs-ansible-roles/forge-server.git
```

Add the role to your `requirements.yaml` file:
```yaml
roles:
  - name: forge_server
    src: "https://gitlab.com/eyegogs-ansible-roles/forge-server"
    scm: git
```

### Example playbook
```
---
- hosts: forge_servers
  become: true

  roles:
    - forge_server
```

## Usage
### Variables
The role requires that at least these variables be set:
- `forge_servers` - The list of minecraft server dictionaries to be deployed in the remote
	- `name` - The name of this minecraft server. The name of the container on the remote will be `mc_{{ server.name }}`.

> **NOTE**: Neither `ops` nor `whitelist` are required attributes for the role to execute correctly.  But note that the servers **whitelist is enabled by default** and must be explictly disabled in `server.properties`.

The entire variable list and defaults are shown below:
`forge_servers_dir` - The directory where the configuration and binaries for the forge servers will be stored. Defaults to `/srv/forge/servers`
`forge_scripts_dir` - The directory for scripts that are used within this role. Defaults to `/srv/forge/scripts`
`forge_overwrite_server_properties` - When set to `true` the `server.properties` file will be templated even it if exists already. (_BREAKS IDEMPOTENCY_) Defaults to `false`.
- `forge_servers` - The list of minecraft server dictionaries to be deployed in the remote
	- `name` - The name of this minecraft server. The name of the container on the remote will be `mc_{{ server.name }}`. No default.
	- `version` - Because there are so many forge and MC versions that are still very popular, there is no default version to deploy. Go to [Dockerhub](https://hub.docker.com/r/eyegog/forge-server/tags) and choose a tag to deploy.
	- `port` - The port number to expose on the container which maps to `25565` inside the container. Defaults to `25565`.
	- `icon` - The path or URL to the 64x64px icon to use for the server. No default.
	- `ops` - The list of usernames to add to `ops.json`. Usernames will be queried against `https://api.mojang.com/users/profiles/minecraft/` to retreive the UUID. And so the server must be ser to _online mode_ for this to work correctly. See `files/minecraft-uuid.py`. No defaults.
	- `whitelist` - The list of usernames to add to `whitelist.json`. Usernames will be queried against `https://api.mojang.com/users/profiles/minecraft/` to retreive the UUID. And so the server must be set to _online mode_ for this to work correctly. See `files/minecraft-uuid.py`. No defaults.
	- `banlist` - The list of usernames to add to `banned-players.json`. Usernames will be queried against `https://api.mojang.com/users/profiles/minecraft/` to retreive the UUID. And so the server must be set to _online mode_ for this to work correctly. See `files/minecraft-uuid.py`. No defaults.
	- `properties` - `properties` is reflective of the `server.properties` minecraft configuration file. This is discussed further in this document. A full list of properties and defaults is availble later in this document.
	- `mod_list` - This is either a path to the mod list on the ansible controller, or a url to download it from. The mod list is a simple text file that contains newline separated URLs to mods to download. Comments are allowed in this file (prefixed with `#`).

#### Plugins
Plugins/mods are defined in a text file called the `mod_list`. It is a simple text file where each string on a new line is a URL to a mod to download onto the forge server. Comments are allowed in this file, they are prefixed with `#`. An example `mod_list` file might contain:

```
# Ars Neuveau
https://mediafilez.forgecdn.net/files/5436/131/ars_nouveau-1.20.1-4.12.1-all.jar
# FTB Chunks
https://mediafilez.forgecdn.net/files/5378/90/ftb-chunks-forge-2001.3.1.jar
```

> The `name` field is for self-documentation and is not reqiured.

##### NOTE - Getting download links for mods
Many mod websites such as [forge](https://www.forgemc.org/resources/) and [curseforge](https://www.curseforge.com/minecraft/mc-mods) have security policies such that [`get_url`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html) runs into `403` errors when trying to retrieve the Java binaries.

There are a couple workarounds for this:
- Many plugins also link to source with CI/CD and/or release pages where you can retrieve alternative download links.
- In your brower you can download the file yourself, view your download history and obtain the download URL that you were redirected to.

A simple config of two minecraft servers, a normal and a hardcore server, might look like this:

```yaml
---
forge_servers:
  - name: normal
    version: "1.12.2-14.23.5.2859"
    ops:
      - someguy
    icon: /path/to/myserver.png
    world: myminecraftworld
    properties:
      motd: "someguy's minecraft server"
    mod_list: "/path/to/mod_list.txt"

  - name: hardcore
    version: "1.20.1-47.3.0"
    ops:
      - someguy
    icon: /path/to/myserver-hardcore.png
    port: 25566
    properties:
      hardcore: true
      motd: "someguy's hardcore minecraft server"
    mod_list: "https://mymodlists.com/this_server.txt"
```

#### `server.properties`
`server.properties` reflects the server.properties conifg file, with a few minor changes to the variable names ('.' replace with '\_' where applicable) and a few exceptions:

- `level-name` - The level name is instead set in `minecraft_servers` as `server.world`. See the above snippet for an example.
- `server-ip` and `server-port`  - The server IP is not set within the container and the default port is `25565` within the container. Used `server.port` in `forge_servers` to set the port on the outside of the container. See the above snippet for an example.

The [Minecraft Wiki](https://minecraft.fandom.com/wiki/Server.properties) already has a comprehensive breakdown of `server.properties` and so it will not be discussed here. A few defaults are different to the defaults of the `server.properties` file. 

**All fields and their defaults are listed below**
- `allow-flight` - (bool) [Default: true]
- `allow-nether` - (bool) [Default: true]
- `broadcast-console-to-ops` - (bool) [Default: true]
- `broadcast-rcon-to-ops` - (bool) [Default: true]
- `difficulty` - (string) [Default: medium] Defines difficulty: `'peaceful'`, `'easy'`, `'medium'`, `'hard'`
- `enable-command-block` - (bool) [Default: true]
- `enable-jmx-monitoring` - (bool) [Default: true]
- `enable-query` - (bool) [Default: false]
- `enable-rcon` - (bool) [Default: false]
- `enable-status` - (bool) [Default: true]
- `enforce-whitelist` - (bool) [Default: true]
- `entity-broadcast-range-percentage` - (int: 10-100) [Default: 100]
- `force-gamemode` - (bool) [Default: false]
- `function-permission-level` - (int: 1-4) [Default: 2]
- `gamemode` - (string) [Default: survival] Default gamemode of the server: `'survival'`, `'creative'`, `'adventure'`, `'spectator'`
- `generate-structures` - (bool) [Default: true]
- `generator-settings` - (string) [Default: {}] The settings used to customize world generation. Follow [its format](https://minecraft.fandom.com/wiki/Java_Edition_level_format#generatorOptions_tag_format "Java Edition level format") and write the corresponding JSON string. Remember to escape all `:` with `\:`.
- `hardcore` - (bool) [Default: false]
- `hide-online-players` - (bool) [Default: false]
- `level-seed` - (string) [Default: _blank_] 
- `level-type` - (string) [Default: minecraft:normal]
- `max-chained-neighbor-update` - (int) [Default: 1000000]
- `max-players` - (int: 0-2^32-1) [Default: 50]
- `max-tick-time` - (int: 0-2^63-1) [Default: 60000]
- `max-world-size` - (int: 1-29999984) [Default: 29999984]
- `motd` - (string) [Default: _A Minecraft Server_]
- `network-compression-threshold` - (int) [Default: 256]
- `online-mode` - (bool) [Default: true]
- `op-permission-level` - (int) [Default: 4]
- `player-idle-timeout` - (int) [Default: 0]
- `prevent-proxy-connections` - (bool) [Default: false]
- `previews-chat` - (bool) [Default: false]
- `pvp` - (bool) [Default: true]
- `query-port` - (int: 1 - 2^16-2) [Default: 25565] Sets the port for the query server
- `rate-limit` - (int) [Default: 0]
- `rcon-password` - (string) [Default: _blank_]
- `rcon-port` - (int: 1 - 2^16-2) [Default: 25575]
- `require-resource-pack` - (bool) [Default: false]
- `resource-pack` - (string) [Default: _blank_]
- `resource-pack-prompt` - (string) [Default: _blank_]
- `resource-pack-sha1` - (string) [Default: _blank_]
- `simulation-distance` - (int: 3-32) [Default: 10]
- `snooper-enabled` - (bool) [Default: false] Sets whether the server sends snoop data regularly to [http://snoop.minecraft.net](http://snoop.minecraft.net).
- `spawn-animals` - (bool) [Default: true]
- `spawn-monsters` - (bool) [Default: true]
- `spawn-npcs` - (bool) [Default: true]
- `spawn-protection` - (int) [Default: 16]
- `sync-chunk-writes` - (bool) [Default: true]
- `use-native-transport` - (bool) [Default: true]
- `view-distance` - (int: 3-32) [Default: 10]
- `white-list` - (bool) [Default: true]
